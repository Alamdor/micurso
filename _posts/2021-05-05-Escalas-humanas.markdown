---
layout: post
title:  "Escalas humanas"
date:   2021-05-05 11:30:00 -0400
categories: actividades
---

Hemos pedido a nuestros estudiantes de Física 1 que tomen sus pulsaciones por minuto y midan la longitud de su cuarta en cm de forma voluntaria y anónima. Usaremos los resultados para reflexionar sobre la idoneidad de usar estas referencias para definir patrones de medida de tiempo y distancia.

Los resultados se analizan en la siguiente [hoja](https://gitlab.com/jalop/micurso/-/blob/master/actividades/EscalasHumanas.ipynb)
