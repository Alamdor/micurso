---
layout: page
title: Preguntas frecuentes
permalink: /fac/
---
## Problemas de acceso al canal de telegram
* El enlace al canal de telegram no abre desde la página web
	* Algunos navegadores no permiten abrir el enlace los canales de telegram desde la dirección http://fisica.ciens.ucv.ve/direccion/cursos/fisicageneral2021/ . La solución es copiar la dirección del enlace (usar el botón derecho del mouse en una computadora, o dejar el enlace presionado unos segundos en una pantalla táctil) y abrirlo en otra pestaña o en la aplicación. 

## Problemas de acceso al campus virtual

### Sobre el correo electrónico
* Algunos servicios de correo electrónico han tenido problemas para recibir los mensajes que se envían desde los servicios de la UCV. Recomendamos revisar frecuentemente la papelera (trash) de su correo o la carpeta de correo no deseado (spam), pues a veces los mensajes son descartados allí. 
* El servicio de hotmail merece una mención aparte. Es frecuente que los mensajes desde la UCV no lleguen de ninguna manera. Recomendamos utilizar un correo alternativo.

### Preguntas
* No puedo acceder al curso, ya que me pide la contraseña la cual no tengo.
	* Pida recuperación de contraseña. Tome en cuenta lo dicho en el apartado **Sobre el correo electrónico**
* No me ha llegado la contraseña / No puedo recuperar mi cuenta del Campus Virtual, no recibo ningún mensaje de cambio de contraseña a mi correo electrónico
	* Revise el apartado **Sobre el correo electrónico**
* No puedo acceder al curso
	* Sí puede ingresar a la plataforma del campus virtual UCV, pero no al curso, exponga su problema en la [encuesta de problemas de acceso](https://forms.gle/XY7MgVgrgS1CZsAEA).
* No puedo responder los foros en los foros, no puedo ver parte del contenido, no me llegan los correos de cuando agregan algún contenido a la plataforma.
	* Revise las notificaciones del foro están activas.
	* Haga la consulta directamente a los instructores del curso.
